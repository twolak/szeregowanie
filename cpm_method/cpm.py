import pandas as pd
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import plotly.express as px


def poprzednicy(zad, Z):
    result = []
    ps = [p[0] for p in paths if p[1] == zad["nazwa"]]
    for z in filter(lambda zs: zs["nazwa"] in ps, Z):
        result.append(
            {key: value for key, value in z.items()})
    return result


def nastepnicy(zad, Z):
    result = []
    ns = [p[1] for p in paths if p[0] == zad["nazwa"]]
    for zs in filter(lambda zss: zss["nazwa"] in ns, Z):
        result.append(
            {key: value for key, value in zs.items()})
    return result


# wyznaczenie ścieżki krytycznej
def get_cpm(graf):
    for z in graf:
        prev = poprzednicy(z)
        if not prev:
            z["min_start"] = 0
        else:
            z["min_start"] = max(p["min_start"] + p["czas"] for p in prev)
        z["min_c"] = z["min_start"] + z["czas"]

    def get_time(z):
        '''Funkcja pomocnicza do sortowania'''
        return z["czas"]

    z = sorted(filter(lambda z: z["min_start"] ==
                      0, graf), key=get_time, reverse=True)[0]

    nxt = nastepnicy(z)
    cpm = [z]
    while nxt:
        z = sorted(nxt, key=get_time, reverse=True)[0]
        cpm.append(z)
        nxt = nastepnicy(z)
    return cpm

# pobranie z pliku
with open("graf.txt", "r") as f:
    tasks_list = {t[0]: int(t[1]) for t in f.readline().split(" ")}
    paths = f.readline().split(" ")

# stworzenie grafu
df = pd.DataFrame({'from': [f'{p[0]}: {tasks_list[p[0]]}' for p in paths],
                'to': [f'{p[1]}: {tasks_list[p[1]]}' for p in paths]})
G = nx.from_pandas_edgelist(df, 'from', 'to', create_using=nx.DiGraph())

# sprawdzenie, czy graf jest cykliczny
if not nx.is_directed_acyclic_graph(G):
    print("Graph jest cykliczny. Wykonywanie algorytmu jest przerwane")
    import sys
    sys.exit(0)


z_list = [{'nazwa' : t, 'czas': tasks_list[t]} for t in tasks_list.keys()]
crit_path = get_cpm(z_list)
graph_duration = max([z["min_start"] + z["czas"] for z in z_list])

node_colors = []
for node in G:
    node_colors.append('red' if node[0] in [n["nazwa"] for n in crit_path] else 'blue')

edge_colors = []
for edge in G.edges():
    edge_colors.append('red' if edge[0][0] in [n["nazwa"] for n in crit_path] else 'blue')

nx.draw_networkx(G, node_size=1000, arrows=True,
                 node_color=node_colors, pos=nx.circular_layout(G), alpha=0.6)
plt.show()
plt.title("Graf")


for z in reversed(z_list):
    nxt = nastepnicy(z)
    if z["nazwa"] in crit_path:
        z["max_c"] = graph_duration
    else:
        z["max_c"] = min(n["max_start"] for n in nxt) if nxt != [] else graph_duration
    z["max_start"] = z["max_c"] - z["czas"]

for z in z_list:
    print(z)

# harmonogram
def print_machines(M):
    i = 1
    for m in M:
        print(f'M{i}: {m}\n')
        i += 1

def get_czas_zak(m):
    return m["czas_zak"]

maszyny = [{
      "zadania": [{
            "nazwa": z_list[0]["nazwa"],
            "czas": z_list[0]["czas"],
            "start": z_list[0]["min_start"]
        }],
      "czas_zak": z_list[0]["min_c"]
    },
    {
        "zadania": [{
            "nazwa": z_list[1]["nazwa"],
            "czas": z_list[1]["czas"],
            "start": z_list[1]["min_start"]
        }],
        "czas_zak": z_list[1]["min_c"]
    },
    {
        "zadania": [{
            "nazwa": z_list[2]["nazwa"],
            "czas": z_list[2]["czas"],
            "start": z_list[2]["min_start"]
        }],
        "czas_zak": z_list[2]["min_c"]
    }]

for z in z_list[3:]:
    z_added = False
    for i in range(len(maszyny)):
        if z["min_start"] >= maszyny[i]["czas_zak"]:            
            maszyny[i]["czas_zak"] = z["min_c"]
            maszyny[i]["zadania"].append({
                "nazwa": z["nazwa"],
                "czas": z["czas"],
                "start": maszyny[i]["czas_zak"] - z["czas"]
                })
            z_added = True
            break
        # jeśli nie mozemy ustawic zadania w jak najwcześniej, 
        # startujemy na maszynie o najmniejszym czasie zakończenia

    if z_added:
        # for m in maszyny:
        #     print(m["czas_zak"], end=' ')
        continue
    # jesli nie możemy wstawić zadania na najwcześniejszy termin
    m = min(maszyny, key=get_czas_zak)
    m["zadania"].append({
        "nazwa": z["nazwa"],
        "czas": z["czas"],
        "start": m["czas_zak"]
    })
    m["czas_zak"] += z["czas"]


print()
print(f'Sciezka krytyczna: {[n["nazwa"] for n in crit_path]}')
print('Harmonogram:')
print_machines(maszyny)





