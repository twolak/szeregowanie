import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt


def nastepnicy(zad):
    result = []
    end_z = [e[1] for e in edges if e[0] == zad["nazwa"]]
    for z in filter(lambda zs: zs["nazwa"] in end_z, z_list):
        result.append(z)
    return result


def get_nazwa(z):
    return z["nazwa"]


def s_list(z):
    return sorted([nxt["etykieta"] for nxt in nastepnicy(z)], reverse=True)


# pobranie z pliku
with open("graf.txt", "r") as f:
    tasks_list = f.readline().split(" ")
    edges = f.readline().split(" ")

# stworzenie grafu
df = pd.DataFrame({'from': [e[0] for e in edges],
                   'to': [e[1] for e in edges]})

G = nx.from_pandas_edgelist(df, 'from', 'to', create_using=nx.DiGraph())
# sprawdzenie, czy graf jest cykliczny
if not nx.is_directed_acyclic_graph(G):
    print("Graph jest cykliczny. Wykonywanie algorytmu jest przerwane")
    import sys

    sys.exit(0)

z_list = [{'nazwa': t[0], 'czas': int(t[1])} for t in tasks_list]

n = len(z_list)
for i in range(n):
    # lista zadań bez etykiet
    z_no_l = list(filter(lambda z: "etykieta" not in z, z_list))
    # wybieramy z nich te, których nastepnicy już mają etykietę
    A = list(filter(lambda z: False not in ["etykieta" in nxt for nxt in nastepnicy(z)], z_no_l))

    for Z in A:
        Z["s_list"] = s_list(Z)

    A.sort(key=s_list)
    # pobieramy zadanie z z_list, aby zmiana sie zapisala
    Z = list(filter(lambda z: z["nazwa"] == A[0]["nazwa"], z_list))[0]
    Z["etykieta"] = i + 1
    print(Z)


def print_schedule(schedule):
    print("M1: ")
    for z in schedule["M1"]:
        print(z)
    print("M2: ")
    for z in schedule["M2"]:
        print(z)


def c_max(schedule):
    m1_c = sum([z["czas"] for z in schedule["M1"]])
    m2_c = sum([z["czas"] for z in schedule["M2"]])
    return max(m1_c, m2_c)


def get_etykieta(z):
    return z["etykieta"]


# sortujemy malejąco wg etykiet, aby zadania były dodane do maszyn w odpowiedniej kolejności
sort_z_list = sorted(z_list, key=get_etykieta, reverse=True)

# wkładamy je po kolei na zmiane do maszyn (wg etykiet malejąco)
sch = {
    "M1": list(filter(lambda z: z["etykieta"] % 2 == 1, sort_z_list)),
    "M2": list(filter(lambda z: z["etykieta"] % 2 == 0, sort_z_list))
}

print("Harmonogram: ")
print_schedule(sch)
print(f'c_max: {c_max(sch)}')

nx.draw_networkx(G, node_size=1000, arrows=True, pos=nx.circular_layout(G), alpha=0.6)
plt.show()
plt.title("Graf")
