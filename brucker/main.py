
tree = []


with open("tree.txt", "r") as file:
    for line in file:
        node = line.strip().split(" ")
        # root nie ma następnika
        is_root = len(node) == 2
        # tree[node[0]] = {
        #     "d": node[1],
        #     "next": None if is_root else node[2],
        #     "root": is_root
        # }
        tree.append({
            "name": node[0],
            "d": int(node[1]),
            "next": None if is_root else node[2],
            "root": is_root
        })


def is_root(n):
    return n["root"]


def next_node(node):
    return list(filter(lambda n: n["name"] == node["next"], tree))[0]


def print_tree(t):
    for node in t:
        if is_root(node):
            print("ROOT", end=" ")
        for item in node.items():
            if item[0] != "root":
                print(f'{item[0]}: {item[1]}', end="\t")
        print()


def set_d(t):
    root = list(filter(lambda n: is_root(n), t))[0]
    root["d*"] = 1 - root["d"]
    for node in list(reversed(t))[1:]:
        node["d*"] = max(1 + next_node(node)["d*"], 1 - node["d"])


set_d(tree)


def get_d(node):
    return node["d*"]

print_tree(tree)